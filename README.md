# DJANGO SONDAGE #


## Simple application de sondage en python se basant sur le modèle MVC

```bash
   cd django-sondage/

   python3 manage.py runserver 
```

visiter http://localhost:8000/sondage/

Pour créer des questions et les choix de réponses, visiter http://localhost:8000/admin/

